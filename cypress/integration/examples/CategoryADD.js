describe('My first test', function () {
	it('Visit the website', function () {
		cy.visit('http://shoppinglist.dev.app.hd.digital/dashboard')

//		Verify website is opened
		cy.contains('Remember')
	
//		Input Email Address and Password and Click Submit
		
		cy.get('.m-login__signin > .m-login__form > :nth-child(2) > .form-control').type('admin@slap.com')
		cy.get('.m-login__signin > .m-login__form > :nth-child(3) > .form-control').type('secret.slap.123!')
		cy.get('button[type=submit]').click()  
		
//		Verify navigation to second page / successful login	
		cy.url()
 		.should('include', '/dashboard/articles')
		
		
//		Clicking on Categories
		cy.get('.m-menu__item--open > .m-menu__submenu > .m-menu__subnav > :nth-child(3) > .m-menu__link > .m-menu__link-text').click()
		
//		Verify categories is opened	
		cy.url()
		.should('include', '/categories')		

//		click on add category		
		cy.get('.btn > :nth-child(1) > span').click()
		
		
			
		cy.get(':nth-child(1) > .col-md-6 > #name').type('category_english')
		cy.get(':nth-child(2) > .col-md-6 > #name').type('category_spanish')
		cy.get(':nth-child(3) > .col-md-6 > #name').type('category_german')
		cy.get(':nth-child(4) > .col-md-6 > #name').type('category_french')
		cy.get(':nth-child(5) > .col-md-6 > #name').type('category_italian')


// SVG File Upload // not working and results in error

//		cy.fixture('images/image.svg').as('image')
//		cy.get('#category-image-file').then($input => {
//       return Cypress.Blob.base64StringToBlob(this.image,
//       'image/svg').then(
//          blob => {
//           const imageFile = new File([blob], 'image.svg', { type:
//            'image/svg' })
//            const dataTransfer = new DataTransfer()
//            dataTransfer.items.add(imageFile)
//            $input[0].files = dataTransfer.files
//			}
//			)
//		})


// workaround as uploading svg File is not supported, manually uploading the svg File while Pause
		cy.pause()
		
		
		
// png file Upload 
		cy.fixture('images/image.png').as('image')
		cy.get('#category-color-image-file').then($input => {
        return Cypress.Blob.base64StringToBlob(this.image,
        'image/png').then(
          blob => {
            const imageFile = new File([blob], 'image.png', { type:
            'image/png' })
            const dataTransfer = new DataTransfer()
            dataTransfer.items.add(imageFile)
            $input[0].files = dataTransfer.files
			}
			)
		})
		
// Click Submit		
		cy.get('.btn-accent').click()
		
	})
})
