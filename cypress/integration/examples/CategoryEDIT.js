describe('My first test', function () {
	it('Visit the website', function () {
		cy.visit('http://shoppinglist.dev.app.hd.digital/dashboard')

//		Verify website is opened
		cy.contains('Remember')
	
//		Input Email Address and Password and Click Submit
		
		cy.get('.m-login__signin > .m-login__form > :nth-child(2) > .form-control').type('admin@slap.com')
		cy.get('.m-login__signin > .m-login__form > :nth-child(3) > .form-control').type('secret.slap.123!')
		cy.get('button[type=submit]').click()  
		
//		Verify navigation to second page / successful login	
		cy.url()
 		.should('include', '/dashboard/articles')
		
		
//		Clicking on Categories
		cy.get('.m-menu__item--open > .m-menu__submenu > .m-menu__subnav > :nth-child(3) > .m-menu__link > .m-menu__link-text').click()
		
//		Verify categories is opened	
		cy.url()
		.should('include', '/categories')		

//		click on edit category		
		cy.get(':nth-child(8) > :nth-child(3) > .m-portlet__nav-link').click()	

		cy.get(':nth-child(1) > .col-md-6 > #name').type('category+_english')
		cy.get(':nth-child(2) > .col-md-6 > #name').type('category_spanish')
		cy.get(':nth-child(3) > .col-md-6 > #name').type('category_german')
		cy.get(':nth-child(4) > .col-md-6 > #name').type('category_french')
		cy.get(':nth-child(5) > .col-md-6 > #name').type('category_italian')
		

// Click Submit		
		cy.get('.btn-accent').click()
		
// Editing and submit also results in Exception		
		
		
	})
})
